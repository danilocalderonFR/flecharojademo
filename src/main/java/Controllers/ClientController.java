/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.Cliente;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author danil
 */
@Stateless
public class ClientController {

  @PersistenceContext
   EntityManager em;
  
  public void addClient(Cliente pCliente){
      em.persist(pCliente);
  }
  public List<Cliente> getAll(){ 
      
      return em.createQuery("Select c from Cliente c order by c.id").getResultList();
      
  }
}
