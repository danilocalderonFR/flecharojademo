/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import Controllers.ClientController;
import Models.Cliente;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.util.List;
import javax.inject.Inject;

/**
 *
 * @author danil
 */
@Named(value = "clientViewManager")
@RequestScoped
public class clientViewManager {
     
@Inject
 ClientController clientController;
 
  private Cliente cliente; 
   
    public clientViewManager() {
      
    }
    
    public Cliente getCliente(){
    if (cliente == null){
        cliente = new Cliente ();
    }
    return cliente;
    }
    
   public void addNewClient(){
//        cliente = new Cliente(Long.valueOf(18),"Silvia");
       clientController.addClient(cliente);
   }
   public List<Cliente> getAllClients(){
       
       
       return clientController.getAll();
   }
}
